use std::fs;

fn main() {
    let contents = read_file("test");
    if let Ok(c) = contents {
        println!("{}", c);
    }
}

fn read_file(file_name: &str) -> Result<String, Box<dyn std::error::Error>> {
    let file = fs::read_to_string(file_name)?;
    Ok(file)
}